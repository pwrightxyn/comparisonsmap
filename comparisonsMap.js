var maxRadius = 30, minRadius = 10, datapoint = 0, theData, allData;
var debug = false;
var apiKey = 'AIzaSyCVGdiUyN2FLLTGVs_xF2czvKPwhsLlQJ0', mapZoom='5', mapWidth = '640', mapHeight = '640'; // map resolution is 2x, so the image delivered is twice as wide and tall
var mapStyle =[
    {
        "featureType": "all",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "weight": "2.00"
            }
        ]
    },
    {
        "featureType": "all",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#9c9c9c"
            }
        ]
    },
    {
        "featureType": "all",
        "elementType": "labels.text",
        "stylers": [
            {
                "visibility": "on"
            }
        ]
    },
    {
        "featureType": "administrative.country",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#fdb838"
            }
        ]
    },
    {
        "featureType": "administrative.country",
        "elementType": "labels.text",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "administrative.province",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#fdb838"
            }
        ]
    },
    {
        "featureType": "administrative.province",
        "elementType": "labels.text",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "administrative.locality",
        "elementType": "labels.text",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "administrative.neighborhood",
        "elementType": "labels.text",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "administrative.land_parcel",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "administrative.land_parcel",
        "elementType": "labels.text",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "all",
        "stylers": [
            {
                "color": "#f2f2f2"
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#ffffff"
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "labels.text",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "landscape.man_made",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#ffffff"
            }
        ]
    },
    {
        "featureType": "landscape.natural",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#ffffff"
            },
            {
                "visibility": "on"
            }
        ]
    },
    {
        "featureType": "landscape.natural",
        "elementType": "labels.text",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "landscape.natural.terrain",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#f0f0f2"
            }
        ]
    },
    {
        "featureType": "landscape.natural.terrain",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "visibility": "off"
            },
            {
                "color": "#f0f0f2"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "labels.text",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "poi.government",
        "elementType": "labels.text",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "poi.park",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#f0eef1"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "all",
        "stylers": [
            {
                "saturation": -100
            },
            {
                "lightness": 45
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#eeeeee"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#7b7b7b"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "labels.text.stroke",
        "stylers": [
            {
                "color": "#ffffff"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "simplified"
            },
            {
                "lightness": "-10"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "all",
        "stylers": [
            {
                "color": "#46bcec"
            },
            {
                "visibility": "on"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#e1e9ec"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#070707"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "labels.text.stroke",
        "stylers": [
            {
                "color": "#ffffff"
            }
        ]
    }
];

function get_static_style(styles) {

    var result = [];
    styles.forEach(function(v, i, a){
        
        var style='';
        if( v.stylers ) { // only if there is a styler object
            if (v.stylers.length > 0) { // Needs to have a style rule to be valid.
                style += (v.hasOwnProperty('featureType') ? 'feature:' + v.featureType : 'feature:all') + '|';
                style += (v.hasOwnProperty('elementType') ? 'element:' + v.elementType : 'element:all') + '|';
                v.stylers.forEach(function(val, i, a){
                    var propertyname = Object.keys(val)[0];
                    var propertyval = val[propertyname].toString().replace('#', '0x');
                    style += propertyname + ':' + propertyval + '|';
                });
            }
        }
        result.push('style='+encodeURIComponent(style));
    });
    
    if(debug)console.log(result.join('&'));
    return result.join('&');
}

var mapURLStr = get_static_style( mapStyle );
var mapSrc = "https://maps.googleapis.com/maps/api/staticmap?center=37.1996445,-111.0380845&zoom="+mapZoom+"&size=" + mapWidth +"x" + mapHeight + "&scale=2&maptype=roadmap&"+mapURLStr+"&key="+apiKey;


// create a scaling radius function
var calcRadius = function(r){
	var calc = d3.scaleLinear()
		.domain([0, xynMax(theData)])
		.range([minRadius, maxRadius]);
	var radius = calc(r);
	if (radius == 0) return 0;
	if (radius < minRadius) return minRadius;
	return radius;
}

var format = function(d){
	var formatFixedPercent = d3.format(".2%"),
	formatPercent = function(x) { return formatFixedPercent(x).replace(/\.0+%$/, "%"); }; // magic.  
	if (!isNaN(d) && d.toString().indexOf('.') != -1)
		return formatPercent(d)
	return d3.format(",")(d);
}


// pass in an array, get the max.
// if it's text, treat it as zero
var xynMax = function(a){
	var newArray = a;
	for (var i = 0; i < a.length; i++){
		newArray[i] = isNaN(a[i]) ? 0 : a[i];
	}
	return d3.max(newArray);	
}



d3.json("/js/BorderplexMapData.json", function(error, data) {
    if(debug)console.log(data); // this is your data
    var titles = data[0];
    var cities = data[1];
    theData = data[datapoint + 2]; // the first two are titles and cities.  Then starts the real data
    allData = data;

    /*var calcRadius = d3.scaleLinear()
		.domain([0, xynMax(theData)])
		.range([minRadius, maxRadius]); */
		
	// Add map url
	var backgroundMap = d3.select('#Map').select('image')
		.attr("xlink:href", mapSrc);
    
    var circle = d3.select('#Circles').selectAll("circle");
    if(debug)console.log(theData);
    circle.data(theData);
    
    // Define the div for the tooltip
	var div = d3.select("body").append("div")	
	    .attr("class", "tooltip")				
	    .style('opacity', 0);
	div.on("click", function(d){
			/* remove the highlight color */
			var thisCircle = d3.select(d3.event.target);
			thisCircle.attr("style", "");
			/* remove the tooltip */
			div.transition()		
                .duration(300)		
                .style("opacity", 0);	
		});	
	    
	var dropDown = d3.select("#DropdownMenuContainer")
		.append("select")
		.attr("id", "MapTitle")
        .attr("name", "Demographics");
    var options = dropDown.selectAll("option")
           .data(titles) 
           .enter()
           .append("option");
    options.text(function (d) { if(debug)console.log("option " + d); return d; })
       .attr("value", function (d, i) { return i + 1; });
    var textOutput = d3.select('#TextDisplay')
    	.append("div");
    	
    /*var hideInfo = function(){
		d3.select('#TextDisplay').style('display', 'none');
		d3.select('#MapOverlay').attr('style', 'background: rgba(0,0,0,0); pointer-events: none;');
	}*/
	var removeOverlay = function(e){
		d3.selectAll('#BackgroundOverlay').remove();
		d3.select('#DataDownload').classed("showBox", false);
		d3.select('#DownloadButton').classed("active", false);
		d3.select('#TextDisplay').classed("showBox", false);
		d3.select('#MapOverlay').classed("fadeOverlay", false);
			/* remove the tooltip */
		div.transition()		
            .duration(300)		
            .style("opacity", 0);	
	};
	var infoButton = d3.select("#InfoButton")
		.on("mouseup", function(){
			textOutput.html(generateTextDisplay(titles[datapoint],theData));
			if (d3.select('#BackgroundOverlay').empty()){
				d3.select('#TextDisplay').classed("showBox", true).style('border-color', "#ccc");
				d3.select('#MapOverlay').classed("fadeOverlay", true);
				var backgroundOverlay = d3.select('#MapOverlay').append('div')
					.attr('id', 'BackgroundOverlay')
					.style('background', 'rgba(0,0,0,.5)')
					.on("click", removeOverlay);
			} else removeOverlay();
		});
	var downloadButton = d3.select("#DownloadButton")
		.on("mouseup", function(){
			if (d3.select('#BackgroundOverlay').empty()){
				d3.select('#DataDownload').classed("showBox", true);
				d3.select('#DownloadButton').classed("active", true);
				var backgroundOverlay = d3.select('#MapOverlay').append('div')
					.attr('id', 'BackgroundOverlay')
					.style('background', 'rgba(0,0,0,.5)')
					.on("click", removeOverlay);
			} else removeOverlay();
		});


    	
   	var generateTextDisplay = function(title, dataArr, options){
	   	var o = (typeof options === "undefined") ? false : options;
		// first do the title
		var output = '<h4><strong>' + title + '</strong></h4><div class="row">\n';
		var leftCol = '<div class="col-xs-6">\n', rightCol = '<div class="col-sm-6 col-xs-4 ">\n';
		for (var i = 0; i < dataArr.length; i++){
			if (o && o == cities[i]){	
				if (format(dataArr[i]) != 0 || (cities[i] != "Juarez" && cities[i] != "Tijuana" &&  cities[i] != "Nogales"))	{
					leftCol +=  "<strong>"+cities[i]+"</strong>";
					rightCol += "<strong>" +format(dataArr[i]) + "</strong>" + '<br>\n';
					leftCol += '<br>\n';
				}
			}else {
				if (format(dataArr[i]) != 0 || (cities[i] != "Juarez" && cities[i] != "Tijuana" &&  cities[i] != "Nogales"))	{
					leftCol += cities[i];
					rightCol += format(dataArr[i]) + '<br>\n';
					leftCol += '<br>\n';
				}
			}

			
		}
		leftCol += '</div>';
		rightCol += '</div>';
		output += leftCol + '\n' + rightCol + '</div>';
		return output;
	};
    	     

	// function updateCircles updates the radii of the circles according to the data selected in the dropdown menu.
	// optional parameter a overrides the dropdown selection (used for the intial state before users have interacted with the dropdown)
    function updateCircles(a) {
	    var selectedValue = (typeof a !== 'undefined') ? a + 1 : d3.event.target.value;  // we use a+1 because usually the value is 1-indexed
	    datapoint = parseInt(selectedValue) - 1; // menu items are 1 indexed.  Datapoint should be 0 indexed 
	    theData = allData[datapoint + 2]; // the first two are titles and cities.  Then starts the real data
        //get the name of the selected option from the change event object
        if(debug)console.log("Selected " + titles[datapoint]); 
        /*calcRadius = d3.scaleLinear()
			.domain([0, xynMax(theData)])
			.range([5, maxRadius]); */
        circle
        	.data(theData)
        	.transition()
			.delay(function(d, i){return 80 * i;})
			.duration(function(d, i){return 1000;})
			.attr("r", function(d, i) { 
		    	var r = -1;
				if (d == "" || isNaN(d))
					return 0;
				else if (d.toString().indexOf('.') != -1) // we have a float
					r = calcRadius(parseFloat(d));
				else r = calcRadius(parseInt(d));
				if(debug)console.log( 'd = ' + d + " converts to " + r + " for " + cities[i]); 
				return r; 
			});
		textOutput.html(generateTextDisplay(titles[datapoint],theData));

        
	}       
	
    dropDown.on("change", updateCircles);
	// run once to start things off
	updateCircles(datapoint);
	
	circle
		.on("mouseover", function(d, i) {
			if(debug)console.log("mouseover: " + d + " i = "  + i);
			
			/* do the highlight color thing */
			var thisCircle = d3.select(d3.event.target);
			var thisColor = d3.rgb(thisCircle.attr("fill"));
			var brighterColor = thisColor.brighter(1);
			if(debug)console.log("mouseover: " + d + "  i = "  + i + " color: " + brighterColor);	
			thisCircle.attr("style", "fill: " + brighterColor + "; z-index: 2") ;
			
			/* do the tooltip thing */
			if (window.innerWidth > 767){
				div.style( 'border-color', thisColor.toString() );
				div.transition()		
	                .duration(200)		
	                .style('opacity', 0.9);
	            div	.html("<strong>" + cities[i] + "</strong>: " +  format(d))	
	                .style("left", Math.min( (d3.event.pageX), (parseInt(d3.select('body').style('width')) - parseInt(div.style('width')))) + "px")		
	                .style("top", (d3.event.pageY - 28) + "px");
			}
			
		}).on("click", function(d, i){
			if(debug)console.log("click: " + d + " i = "  + i);
			div.style('opacity', 0.9);

			
			/* do the highlight color thing */
			var thisCircle = d3.select(d3.event.target);
			var thisColor = d3.rgb(thisCircle.attr("fill"));
			var brighterColor = thisColor.brighter(1);
			if(debug)console.log("mouseover: " + d + "  i = "  + i + " color: " + brighterColor);	
			thisCircle.attr("style", "fill: " + brighterColor + "; z-index: 2") ;
			
				textOutput.html(generateTextDisplay(titles[datapoint],theData,cities[i]));
				if (d3.select('#BackgroundOverlay').empty()){
					d3.select('#TextDisplay').classed("showBox", true).style('border-color', thisColor);
					d3.select('#MapOverlay').classed("fadeOverlay", true);
					var backgroundOverlay = d3.select('#MapOverlay').append('div')
						.attr('id', 'BackgroundOverlay')
						.style('background', 'rgba(0,0,0,.5)')
						.on("click", removeOverlay);
				} else removeOverlay();

		})
		.on("mouseout", function(d){
			/* remove the highlight color */
			var thisCircle = d3.select(d3.event.target);
			thisCircle.attr("style", "");
			/* remove the tooltip */
			div.transition()		
                .duration(300)		
                .style("opacity", 0);	
		});	
	
});

